var clickcountnumber = 1
var starttingtime =0
var endingtime = 1
var durate = 0
var money = 0

let Click = () =>{
    let play = document.getElementById('play')
    let stop = document.getElementById('stop')
    let clear = document.getElementById('clear')
    clickcountnumber++

    let c = document.getElementById('button-start-stop-clear')  
    if(clickcountnumber == 1){
        c.classList.remove('btn-warning')
        c.classList.add('btn-success')
        stop.style.display = "none"
        clear.style.display = "none"
        play.style.display = "inline-block"
        document.getElementById('start-time').innerHTML = "0:00"
        document.getElementById('end-time').innerHTML = "0:00"
        document.getElementById('duration').innerHTML = "0"
        document.getElementById('money').innerHTML = "0"
    }
    else if(clickcountnumber == 2){
        c.classList.remove('btn-success')
        c.classList.add('btn-danger')
        play.style.display = "none"
        clear.style.display = "none"
        stop.style.display = "inline-block"
        let datetime = new Date();
        let start = datetime.toTimeString().split(' ')
        starttingtime = start[0]
        document.getElementById('start-time').innerHTML = formatAMPM(datetime)
    }
    else if(clickcountnumber == 3){
        c.classList.remove('btn-danger')
        c.classList.add('btn-warning')
        play.style.display = "none"
        stop.style.display = "none"
        clear.style.display = "inline-block"

        let datetime = new Date()
        let end = datetime.toTimeString().split(' ')
        endingtime = end[0]
        document.getElementById('end-time').innerHTML = formatAMPM(datetime)
        durate = durations(starttingtime, endingtime)
        document.getElementById('duration').innerHTML = durate
        clickcountnumber = 0

        //Generate money to pay
        let durationdivide = Math.floor(durate/60)
        let durationmadulo = durate%60
        if(durationdivide <= 1 ){
            if(durate<=15 && durate>=0)
                money = 500
            else if(durate <=30)
                money = 1000
            else if(durate <=60)
                money = 1500
            else money =2000
        }
        else{
            money = durationdivide*2000 
            if(durationmadulo<=15 && durationmadulo>=0)
                money = money + 500
            else if(durationmadulo <=30)
                money = money + 1000
            else if(durationmadulo <60)
                money = money + 1500
        }

        document.getElementById('money').innerHTML = money
    }
}

let formatAMPM = date =>{
    let hours = date.getHours()
    let minutes = date.getMinutes()
    let ampm = hours >= 12 ? 'PM' : 'AM'
    hours = hours > 12 ? hours % 12 : hours
    minutes = minutes < 10 ? '0'+minutes : minutes
    return hours + ':' + minutes + ' ' + ampm
  }

let durations = (start, end) =>{
    start = start.split(":");
    end = end.split(":");
    console.log(start[0]);
    let starttime = parseInt( start[0]*60) + parseInt( start[1])
    let endtime = parseInt( end[0]*60) + parseInt( end[1])
    return endtime - starttime
}


setInterval (() =>{
    let datetime = new Date()
    let localtime = datetime.toLocaleTimeString()
    let date = datetime.toString().split(' ')
    let nowdate = date[0] + " " + date[1] + " " + date[2] + " " + date[3] + " " + localtime
    document.getElementById('date-form').innerHTML = nowdate
},1000)




